<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/api', function () use ($router) {
    return $router->app->version();
});

$router->get('/api/karas', [
    'middleware' => 'throttle:60,1',
    'uses' => 'KaraokeController@getKaras'
]);

$router->get('/api/karas/random', [
    'middleware' => 'throttle:60,1',
    'uses' => 'KaraokeController@getRandomKaras'
]);

$router->post('/api/karas/{kara}', [
    'middleware' => 'throttle:15,1',
    'uses' => 'KaraokeController@postKara'
]);

$router->delete('/api/karas/{kara}', [
    'middleware' => 'throttle:15,1|auth',
    'uses' => 'KaraokeController@removeKara'
]);

$router->post('/api/auth', [
    'middleware' => 'throttle:15,1',
    'uses' => 'AuthController@checkAuth'
]);
