import { Store } from 'vuex'
import { getModule } from 'vuex-module-decorators'
import karas from '~/store/karas'
import likes from "~/store/likes";
import admin from "~/store/admin";

let karasStore: karas
let likesStore: likes
let adminStore: admin

function initialiseStores(store: Store<any>): void {
    karasStore = getModule(karas, store)
    likesStore = getModule(likes, store)
    adminStore = getModule(admin, store)
}

export { initialiseStores, karasStore, likesStore, adminStore }
