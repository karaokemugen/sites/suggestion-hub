// Translation: luclu7 (pls don't blame me)
// Well QCTX blames you! ^^ (#13)
export default {
    header: {
        title: "Karaoke Mugen import suggestions",
        random: "Random selection",
        search: "Search",
        add: "Suggest karaoke",
        about: "About",
        km: "Karaoke Mugen's website"
    },
    kara: {
        yes: "Yes!",
        ok: "All good!",
        last: {
            text: "You still can't find?",
            btn: "Suggest it!"
        },
        remove: "Remove"
    },
    random: {
        title: "Karaoke Mugen import suggestions",
        description: "5 random karaokes will be displayed, choose the ones you like see integrated later in Karaoke Mugen and reroll the dices if you want others.",
        relaunch: "Reroll the dices"
    },
    search: {
        title: "Karaoke Mugen import suggestions",
        description: "Use the search to find a karaoke you would like to see integrated later in Karaoke Mugen.",
        placeholder: "Search",
        sort: {
            most_likes: "Most likes",
            a_z: "A to Z",
            least_likes: "Least likes"
        },
        next: "Next page",
        previous: "Previous page",
        aria: {
            goto: "Go to page {0}",
            page: "Page {0}",
            sort: "Sort by"
        }
    },
    modal: {
        welcome: {
            title: "Hello!",
            subtitle: "Where am I?",
            text: "There's a lot of other karaokes databases: ones for UltraStar, Karawin or Karafun softwares. So here are some titles which are already available, with their lyrics synchronized, but has we can't integrate immediately everything, we'd like to know your priorities!",
            text2: "You can either {search}, or {random} do the job. In both cases, we'd be grateful for the time you spend enhancing our databases. Thank you!",
            search: "search something",
            random: "let the random",
            text3: "If you want to ask for help, come and see us on {discord}. We're listening your suggestions and critics.",
            discord: "Discord",
            btn: "OK!"
        },
        suggest: {
            title: "Suggestion",
            subtitle: "You couldn't find what you've searched for? You can still fill a suggestion ",
            fields: {
                title: {
                    label: "Title",
                    placeholder: "JINGO JUNGLE"
                },
                series: {
                    label: "Series / Singer",
                    placeholder: "Yôjo Senki: Saga of Tanya the Evil"
                },
                type: {
                    label: "Type",
                    select: {
                        op: "Opening",
                        cs: "Character Song",
                        is: "Image Song",
                        in: "Insert Song",
                        ed: "Ending",
                        cm: "Commercial",
                        live: "Live",
                        mv: "Movie Video",
                        amv: "AMV",
                        pv: "Promotional Video",
                        ot: "Autre"
                    }
                },
                link: {
                    label: "Link",
                    placeholder: "https://www.youtube.com/watch?v=5VRyiaszGtA"
                },
                name: {
                    label: "Your name",
                    placeholder: "Magic anonymous"
                }
            },
            submit: "Submit",
            submitted: {
                subtitle: "Message heard loud and clear!",
                text: "Your suggestion was received, you can check its status by clicking {here}.",
                here: "here",
                close: "Fermer"
            }
        }
    },
    auth: {
        title: "Login",
        subtitle: "Are you Aeden, AxelTerizaki, Florent Berthelot or even Guillaume Lebigot?",
        text: "Login using the private token. Go away if you're not authorized!",
        field: {
            label: "Token",
            placeholder: "Bananas are good"
        },
        btn: "Login",
        dis: "Logout",
        err: "Invalid token"
    }
}
