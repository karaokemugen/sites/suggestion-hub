export default {
    header: {
        title: "Suggestions d'import Karaoke Mugen",
        random: "Sélection aléatoire",
        search: "Recherche",
        add: "Suggérer karaoké",
        about: "À propos",
        km: "Site web de Karaoke Mugen"
    },
    kara: {
        yes: "Oui !",
        ok: "C'est noté !",
        last: {
            text: "Vous ne trouvez toujours pas ?",
            btn: "Suggérez-le nous !"
        },
        remove: "Supprimer"
    },
    random: {
        title: "Suggestions d'import Karaoke Mugen",
        description: "5 karaokés vont vous être présentés au hasard parmi notre liste, choisissez ceux qui vous plaisent et relancez les dés si vous voulez en avoir d'autres.",
        relaunch: "Relancer les dés"
    },
    search: {
        title: "Suggestions d'import Karaoke Mugen",
        description: "Utilisez la recherche pour trouver vos karaokés préférés.",
        placeholder: "Rechercher",
        sort: {
            most_likes: "Le plus de likes",
            a_z: "De A à Z",
            least_likes: "Le moins de likes"
        },
        next: "Page suivante",
        previous: "Page précédente",
        aria: {
            goto: "Aller à la page {0}",
            page: "Page {0}",
            sort: "Trier par"
        }
    },
    modal: {
        welcome: {
            title: "Bonjour !",
            subtitle: "Où suis-je donc ?",
            text: "Il y a de nombreuses autres bases de karaokés à travers le monde : notamment celles se concentrant sur la réalisation de karaokés d'UltraStar, Karawin ou Karafun. Voici donc, des karaokés qui sont déjà disponibles, avec leurs paroles correctement synchronisées, mais comme on ne peut pas tout faire, on aimerait bien connaître vos priorités !",
            text2: "Vous pouvez soit {search}, soit {random} vous présenter ce qu'il trouve pour vous. Dans les deux cas, nous vous serons reconnaissants du temps que vous passez pour améliorer nos bases. Merci !",
            search: "effectuer une recherche",
            random: "laisser le hasard",
            text3: "Si vous souhaitez nous demander de l'aide, pensez à venir nous voir sur {discord}. Nous sommes à l'écoute de vos suggestions ou critiques.",
            discord: "Discord",
            btn: "Compris !"
        },
        suggest: {
            title: "Suggestion",
            subtitle: "Vous n'avez pas trouvé votre bonheur ? Vous pouvez quand même nous formuler une recommandation.",
            fields: {
                title: {
                    label: "Titre",
                    placeholder: "JINGO JUNGLE"
                },
                series: {
                    label: "Série / Chanteur",
                    placeholder: "Yôjo Senki: Saga of Tanya the Evil"
                },
                type: {
                    label: "Type",
                    select: {
                        op: "Opening",
                        cs: "Character Song",
                        is: "Image Song",
                        in: "Insert Song",
                        ed: "Ending",
                        cm: "Publicité",
                        live: "Concert",
                        mv: "Movie Video",
                        amv: "AMV",
                        pv: "Vidéo promotionnelle",
                        ot: "Autre"
                    }
                },
                link: {
                    label: "Lien",
                    placeholder: "https://www.youtube.com/watch?v=5VRyiaszGtA"
                },
                name: {
                    label: "Votre nom",
                    placeholder: "Anonyme magique"
                }
            },
            submit: "Envoyer",
            submitted: {
                subtitle: "Message reçu !",
                text: "Votre suggestion a été reçue, vous pouvez voir le suivi de cette suggestion en cliquant {here}.",
                here: "ici",
                close: "Fermer"
            }
        }
    },
    auth: {
        title: "Se connecter",
        subtitle: "Êtes-vous Aeden, AxelTerizaki, Florent Berthelot ou encore Guillaume Lebigot ?",
        text: "Connectez-vous en utilisant le jeton privé. Fuyez si vous n'êtes pas autorisé !",
        field: {
            label: "Jeton",
            placeholder: "C'est bon les bananes"
        },
        btn: "Se connecter",
        dis: "Se déconnecter",
        err: "Jeton invalide"
    }
}
