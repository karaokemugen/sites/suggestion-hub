import { Module, VuexModule, Mutation } from 'vuex-module-decorators'

@Module({
    name: 'karas',
    stateFactory: true,
    namespaced: true,
})
export default class Karas extends VuexModule {
    karas = {} as {[index:number]:any}
    lastPage = 1 as number

    @Mutation
    setPage(payload: [number, object]) {
        this.karas[payload[0]] = payload[1];
    }

    @Mutation
    setLastPage(page: number) {
        this.lastPage = page;
    }

    @Mutation
    clearStore() {
        this.karas = {};
        this.lastPage = 0;
    }
}
