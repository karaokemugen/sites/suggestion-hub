import { Module, VuexModule, Mutation } from 'vuex-module-decorators'

@Module({
    name: 'admin',
    stateFactory: true,
    namespaced: true,
})
export default class Karas extends VuexModule {
    auth = ''

    @Mutation
    setAuth(payload: string) {
        this.auth = payload;
    }
}
