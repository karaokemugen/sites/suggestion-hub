# Suggestion Hub

A WIP project to let users choose their favorites karaokes from the "to be imported" list

## A quick deploy manual

- Install dependencies

```Bash
yarn
composer install --no-dev
```

- Write .env variables

```Bash
cp .env.example .env
$EDITOR .env
```

- Generate frontend

```Bash
yarn generate
```

- Deploy Apache on `public/` folder. (.htaccess is not tested tho)

The index.html should be the only file in `index` directive.

## Command reference

With `artisan`, you can import datasets to the database.

```Bash
# Import the file to the DB
# (append --duplicates to skip duplicates check)
$ php artisan kara:import <file>
# Export the DB to the file
$ php artisan kara:export <file> 
```

The `source` column is defaulted to the file name.

### Formats
The file passed to `kara:import` command may be a simple line-by-line txt format.
It may be a JSON formatted as an array of strings or as an array of Karaoke object.

```JSON
[
  "file 1",
  "file 2"
]
```

```JSON
[
  {"name": "file 1", "source": "Ultrastar ES"},
  {"name": "file 2", "source": "Ultrastar ES"}
]
```
