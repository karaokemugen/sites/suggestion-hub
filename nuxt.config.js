export default {
    mode: 'spa',
    /*
    ** Dirs mappings
    */
    srcDir: 'nuxt/',
    buildDir: '.nuxt',
    /*
    ** Headers of the page
    */
    head: {
        title: 'Suggestions d\'import - Karaoke Mugen',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: process.env.npm_package_description || ''}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
        ],
        htmlAttrs: {
            class: 'has-navbar-fixed-top'
        }
    },
    /*
    ** Customize the progress-bar color
    */
    loading: {color: '#fff'},
    /*
    ** Global CSS
    */
    css: [
        {src: '~/assets/main.scss', lang: 'scss'},
        '@fortawesome/fontawesome-svg-core/styles.css'
    ],
    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        '~/plugins/icons.js',
        {src: '~/plugins/vuex-persist.js', ssr: false}
    ],
    /*
    ** Nuxt.js dev-modules
    */
    buildModules: [
        '@nuxt/typescript-build'
    ],
    /*
    ** Nuxt.js modules
    */
    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        // Doc: https://github.com/nuxt-community/dotenv-module
        ['@nuxtjs/dotenv', {path: process.cwd()}],
        // Doc: https://nuxt-community.github.io/nuxt-i18n
        ['nuxt-i18n', {
            locales: [
                {
                    code: 'en',
                    name: 'English',
                    iso: 'en',
                    file: 'en.ts'
                },
                {
                    code: 'fr',
                    name: 'Français',
                    iso: 'fr',
                    file: 'fr.ts'
                }
            ],
            baseUrl: process.env.APP_URL,
            seo: true,
            lazy: true,
            defaultLocale: 'fr',
            strategy: 'prefix_and_default',
            langDir: 'lang/',
            vuex: false
        }]
    ],
    /*
    ** Axios module configuration
    ** See https://axios.nuxtjs.org/options
    */
    axios: {},
    /*
    ** Build configuration
    */
    build: {
        postcss: {
            preset: {
                features: {
                    customProperties: false
                }
            }
        },
        /*
        ** You can extend webpack config here
        */
        extend(config, ctx) {
        }
    },

    generate: {
        dir: 'public/'
    }
}
