<?php


namespace App\Library;


use App\Karaoke;
use GrahamCampbell\GitLab\Facades\GitLab;

class GitLabUtil
{
    public static function createIssue(Karaoke $kara) {
        if (!env('GITLAB_PROJECT_ID', false)) return null;
        $response = GitLab::api('issues')->create(env('GITLAB_PROJECT_ID'), [
            'title' => '[Suggestion hub] '.$kara->filename,
            'description' => "Ce karaoké a atteint plus de ".$kara->likes." likes, c'est un karaoké demandé !\n\nSource : ".$kara->source,
            'labels' => 'suggestion'
        ]);
        $kara->gitlab_issue_id = $response['iid'];
        $kara->save();
        return true;
    }

    public static function shouldIssueBeCreated(Karaoke $kara) {
        if ($kara->gitlab_issue_id) return false;
        // Le but de ce petit algo est de s'adapter progressivement à la montée de l'utilisation de la plateforme
        // Une issue devrait être créée si un karaoké a au moins 70% des likes du kara le plus adoré par les utilisateurs.
        $max = Karaoke::orderBy('likes', 'desc')->first()->likes;
        $threshold = round($max * 0.7);
        return $kara->likes > $threshold;
    }
}
