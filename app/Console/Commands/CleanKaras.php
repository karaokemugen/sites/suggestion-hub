<?php


namespace App\Console\Commands;


use App\Karaoke;
use Illuminate\Console\Command;
use Transliterator;

class CleanKaras extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kara:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean karaokes by using array_unique method: time-consuming';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Starting to remove duplicates, please be patient...');
        $transliterator = Transliterator::createFromRules(':: NFD; :: [:Nonspacing Mark:] Remove; :: Lower(); :: NFC;', Transliterator::FORWARD);
        $karas = Karaoke::select(['id', 'filename'])->get();
        $translit = [];
        foreach ($karas as $kara) {
            $translit[$kara->id] = $transliterator->transliterate($kara->filename);
        }
        $count = 0;
        $translit_unique = array_unique($translit, SORT_STRING);
        $this->info('Done! Removing...');
        foreach ($translit as $key => $title) {
            if (empty($translit_unique[$key])) {
                Karaoke::find($key)->delete();
                $count = $count + 1;
            }
        }
        $this->info('Removed '.$count.' karas!');
        return true;
    }
}
