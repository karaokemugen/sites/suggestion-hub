<?php


namespace App\Console\Commands;

use App\Karaoke;
use Illuminate\Console\Command;

class ExportKaras extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kara:export
                            {file : File to export}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export file to JSON';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filename = $this->argument('file');
        $file = fopen($filename, 'w');
        $out = [];
        foreach (Karaoke::select(['filename', 'source'])->get() as $kara) {
            $out[] = ['name' => $kara->filename, 'source' => $kara->source];
        }
        fwrite($file, json_encode($out, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        fclose($file);
        $this->info('Exported karaokes to '.$filename);
        return true;
    }
}
