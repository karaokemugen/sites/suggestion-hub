<?php


namespace App\Console\Commands;

use App\Karaoke;
use Illuminate\Console\Command;

class ImportKaras extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kara:import
                            {file : File to import from (JSON or txt)}
                            {--duplicates : disable check for duplicates}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import file to database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filename = $this->argument('file');
        $duplicates = $this->option('duplicates');
        $file = fopen($filename, 'r');
        $content = fread($file, filesize($filename));
        fclose($file);
        $arr = json_decode($content, true);
        if (empty($arr)) {
            $arr = explode(PHP_EOL, $content);
        }
        $this->info('Importing ' . count($arr) . ' files from '. $filename . '...');
        $bar = $this->output->createProgressBar(count($arr));
        foreach ($arr as $item) {
            if (is_array($item)) {
                $el = $item['name'];
                $source = $item['source'];
            } else {
                $el = $item;
                $source = $filename;
            }
            if ($duplicates) {
                Karaoke::create([
                    'filename' => $el,
                    'source' => $source
                ]); // Create without asking questions
            } else {
                // Lookup database for a duplicate entry
                $kara = Karaoke::where('filename', $el)->first();
                if (empty($kara)) {
                    Karaoke::create([
                        'filename' => $el,
                        'source' => $source
                    ]);
                } else {
                    $bar->clear();
                    $this->comment($el.' was skipped, it is already in the database.');
                    $bar->display();
                }
            }
            $bar->advance();
        }
        $bar->finish();
        return;
    }
}
