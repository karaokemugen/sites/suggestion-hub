<?php

namespace App\Http\Controllers;

use App\Karaoke;
use App\Library\GitLabUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Validation\Rule;

class KaraokeController extends Controller
{

    /**
     * List karas
     *
     * @param Request $request
     * @return mixed
     */
    public function getKaras(Request $request) {
        $data = $this->validate($request, [
            'q' => 'nullable|string',
            'sort' => ['required', Rule::in(['likes', 'likes_asc', 'az'])]
        ]);
        $page_command = 'paginate';
        if ($request->input('page') > 1) {
            $page_command = 'simplePaginate';
        }
        $filter = '';
        $order = '';
        switch ($data['sort']) {
            case 'likes':
                $filter = 'likes';
                $order = 'desc';
                break;
            case 'likes_asc':
                $filter = 'likes';
                $order = 'asc';
                break;
            case 'az':
                $filter = 'filename';
                $order = 'asc';
                break;
        }
        if (empty($data['q'])) {
            return Karaoke::orderBy($filter, $order)
                ->{$page_command}(20, ['id', 'filename', 'likes', 'source']);
        } else {
            // Generate a array with queries in a SQL syntax
            $where = [];
            foreach (explode(' ', $data['q']) as $word) {
                if (Schema::getConnection()->getDriverName() === 'pgsql') {
                    $where[] = ['filename', 'ilike', '%'.strtolower($word).'%'];
                } else {
                    $where[] = ['filename', 'like', '%'.strtolower($word).'%'];
                }
            }
            return Karaoke::where($where)
                ->orderBy($filter, $order)
                ->{$page_command}(20, ['id', 'filename', 'likes', 'source']);
        }
    }

    /**
     * Generate a random list
     *
     * @return mixed
     */
    public function getRandomKaras(Request $request) {
        return Karaoke::inRandomOrder()->limit(5)->get();
    }

    /**
     * Add a like or a duplicate marker
     *
     * @param Request $request
     * @param integer $kara
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function postKara(Request $request, $kara) {
        $kara = Karaoke::findOrFail($kara);
        $data = $this->validate($request, [
            'type' => ['required', Rule::in(['like'])]
        ]);
        $kara->increment($data['type'].'s');
        $kara->save();
        if (GitLabUtil::shouldIssueBeCreated($kara)) {
            GitLabUtil::createIssue($kara);
        }
        return $kara;
    }

    /**
     * Delete a karaoke from database
     * It can be because it's already in repos or to resolve duplicates
     *
     * @param integer $kara
     * @return mixed
     */
    public function removeKara($kara) {
        $kara = Karaoke::findOrFail($kara);
        return response()->json($kara->delete());
    }

}
