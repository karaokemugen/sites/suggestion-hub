<?php


namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Check auth
     *
     * @return \Illuminate\Http\Response
     */
    public function checkAuth() {
        if (Auth::guest()) {
            return response('No.', 401);
        } else {
            return response('Yea.');
        }
    }
}
