<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Karaoke;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Karaoke::class, function (Faker $faker) {
    return [
        'filename' => $faker->userName.'.ass',
        'likes' => $faker->randomNumber(5),
        'duplicates' => $faker->randomNumber(5)
    ];
});
