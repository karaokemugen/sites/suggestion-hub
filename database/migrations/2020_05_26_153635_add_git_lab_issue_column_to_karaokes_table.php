<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGitLabIssueColumnToKaraokesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('karaokes', function (Blueprint $table) {
            $table->integer('gitlab_issue_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('karaokes', function (Blueprint $table) {
            $table->dropColumn('gitlab_issue_id');
        });
    }
}
