<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveDuplicatesColumnOnKaraokesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::getConnection()->getDriverName() != 'sqlite') {
            Schema::table('karaokes', function (Blueprint $table) {
                $table->dropColumn('duplicates');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::getConnection()->getDriverName() != 'sqlite') {
            Schema::table('karaokes', function (Blueprint $table) {
                $table->integer('duplicates')->default(0);
            });
        }
    }
}
